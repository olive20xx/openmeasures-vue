export function downloadBase64 (base64) {
  const a = document.createElement('a')
  a.href = base64
  a.download = 'open-measures-chart.png'
  a.click()
}
