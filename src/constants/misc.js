const PRODUCTION = 'production'

module.exports = {
  PRODUCTION,
  ENV: (
    process.env.VUE_APP_ENV_DISPLAY ||
      process.env.NODE_ENV ||
      PRODUCTION
  ),

  HOME: 'home',
}
