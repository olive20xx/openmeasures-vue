'use strict'

module.exports = {
  REDDIT: 'reddit',
  GAB: 'gab',
  FOURCHAN: '4chan',
  EIGHTKUN: '8kun',
  PARLER: 'parler'
}
